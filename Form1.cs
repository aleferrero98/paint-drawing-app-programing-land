namespace Paint_Programing_Land
{
    public partial class MyPaint : Form
    {
        // Bitmap bitmap = new Bitmap(1920, 1080);
        Pen pen = new Pen(Color.Black, 5);
        bool isDrawing = false;
        int penSize = 5;
        Image openedFile;

        public MyPaint()
        {
            InitializeComponent();

            Bitmap bitmap = new Bitmap(1920, 1080);
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    bitmap.SetPixel(i, j, Color.White);
                }
            }
            openedFile = bitmap;
        }

        private void Board_PB_MouseDown(object sender, MouseEventArgs e)
        {
            isDrawing = true;
        }

        private void Board_PB_MouseUp(object sender, MouseEventArgs e)
        {
            isDrawing = false;
        }

        private void Board_PB_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDrawing)
            {
                Graphics graphics = Graphics.FromImage(openedFile);
                graphics.DrawRectangle(pen, e.X, e.Y, penSize, penSize);
                Board_PB.Image = openedFile;
            }
        }

        private void Black_Btn_Click(object sender, EventArgs e)
        {
            pen.Color = Color.Black;
        }

        private void Red_Btn_Click(object sender, EventArgs e)
        {
            pen.Color = Color.Red;
        }

        private void Gold_Btn_Click(object sender, EventArgs e)
        {
            pen.Color = Color.Gold;
        }

        private void PenSize_5_Click(object sender, EventArgs e)
        {
            penSize = 5;
        }

        private void PenSize_10_Click(object sender, EventArgs e)
        {
            penSize = 10;
        }

        private void PenSize_15_Click(object sender, EventArgs e)
        {
            penSize = 15;
        }

        private void Save_MenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Jpeg Image (*.jpg)|*.jpg|Bitmap Image (*.bmp)|*.bmp";
            saveFileDialog.Title = "Save an Image File";

            if ( saveFileDialog.ShowDialog() == DialogResult.OK && saveFileDialog.FileName != "")
            {
                System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog.OpenFile();
                switch (saveFileDialog.FilterIndex)
                {
                    case 1:
                        this.Board_PB.Image.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
                        break;
                    case 2:
                        this.Board_PB.Image.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);
                        break;
                }
                fs.Close();
                MessageBox.Show("Image saved sucessfully...");
            }
        }

        private void Open_MenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            DialogResult dr = op.ShowDialog();
            if (dr == DialogResult.OK)
            {
                openedFile = Image.FromFile(op.FileName);
                this.Board_PB.Image = openedFile;
            }

        }
    }
}