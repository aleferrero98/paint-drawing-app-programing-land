﻿namespace Paint_Programing_Land
{
    partial class MyPaint
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            menuStrip1 = new MenuStrip();
            toolStripMenuItem1 = new ToolStripMenuItem();
            Save_MenuItem = new ToolStripMenuItem();
            Open_MenuItem = new ToolStripMenuItem();
            toolStrip1 = new ToolStrip();
            Black_Btn = new ToolStripButton();
            Red_Btn = new ToolStripButton();
            Gold_Btn = new ToolStripButton();
            PenSize = new ToolStripDropDownButton();
            PenSize_5 = new ToolStripMenuItem();
            PenSize_10 = new ToolStripMenuItem();
            PenSize_15 = new ToolStripMenuItem();
            Board_PB = new PictureBox();
            menuStrip1.SuspendLayout();
            toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)Board_PB).BeginInit();
            SuspendLayout();
            // 
            // menuStrip1
            // 
            menuStrip1.Items.AddRange(new ToolStripItem[] { toolStripMenuItem1 });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(800, 24);
            menuStrip1.TabIndex = 0;
            menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            toolStripMenuItem1.DropDownItems.AddRange(new ToolStripItem[] { Save_MenuItem, Open_MenuItem });
            toolStripMenuItem1.Name = "toolStripMenuItem1";
            toolStripMenuItem1.Size = new Size(37, 20);
            toolStripMenuItem1.Text = "File";
            // 
            // Save_MenuItem
            // 
            Save_MenuItem.Name = "Save_MenuItem";
            Save_MenuItem.Size = new Size(103, 22);
            Save_MenuItem.Text = "Save";
            Save_MenuItem.Click += Save_MenuItem_Click;
            // 
            // Open_MenuItem
            // 
            Open_MenuItem.Name = "Open_MenuItem";
            Open_MenuItem.Size = new Size(103, 22);
            Open_MenuItem.Text = "Open";
            Open_MenuItem.Click += Open_MenuItem_Click;
            // 
            // toolStrip1
            // 
            toolStrip1.Items.AddRange(new ToolStripItem[] { Black_Btn, Red_Btn, Gold_Btn, PenSize });
            toolStrip1.Location = new Point(0, 24);
            toolStrip1.Name = "toolStrip1";
            toolStrip1.Size = new Size(800, 31);
            toolStrip1.TabIndex = 1;
            toolStrip1.Text = "toolStrip1";
            // 
            // Black_Btn
            // 
            Black_Btn.AutoSize = false;
            Black_Btn.BackColor = SystemColors.ActiveCaptionText;
            Black_Btn.DisplayStyle = ToolStripItemDisplayStyle.Text;
            Black_Btn.ForeColor = SystemColors.ButtonHighlight;
            Black_Btn.ImageTransparentColor = Color.Magenta;
            Black_Btn.Name = "Black_Btn";
            Black_Btn.Size = new Size(50, 28);
            Black_Btn.Text = "Black";
            Black_Btn.ToolTipText = "Color negro";
            Black_Btn.Click += Black_Btn_Click;
            // 
            // Red_Btn
            // 
            Red_Btn.AutoSize = false;
            Red_Btn.BackColor = Color.Red;
            Red_Btn.DisplayStyle = ToolStripItemDisplayStyle.Text;
            Red_Btn.ForeColor = SystemColors.ButtonHighlight;
            Red_Btn.ImageTransparentColor = Color.Magenta;
            Red_Btn.Name = "Red_Btn";
            Red_Btn.Size = new Size(50, 28);
            Red_Btn.Text = "Red";
            Red_Btn.ToolTipText = "Color rojo";
            Red_Btn.Click += Red_Btn_Click;
            // 
            // Gold_Btn
            // 
            Gold_Btn.AutoSize = false;
            Gold_Btn.BackColor = Color.Yellow;
            Gold_Btn.DisplayStyle = ToolStripItemDisplayStyle.Text;
            Gold_Btn.ForeColor = SystemColors.ActiveCaptionText;
            Gold_Btn.ImageTransparentColor = Color.Magenta;
            Gold_Btn.Name = "Gold_Btn";
            Gold_Btn.Size = new Size(50, 28);
            Gold_Btn.Text = "Gold";
            Gold_Btn.ToolTipText = "Color dorado";
            Gold_Btn.Click += Gold_Btn_Click;
            // 
            // PenSize
            // 
            PenSize.AutoSize = false;
            PenSize.DisplayStyle = ToolStripItemDisplayStyle.Text;
            PenSize.DropDownItems.AddRange(new ToolStripItem[] { PenSize_5, PenSize_10, PenSize_15 });
            PenSize.ImageTransparentColor = Color.Magenta;
            PenSize.Name = "PenSize";
            PenSize.Size = new Size(90, 28);
            PenSize.Text = "PenSize";
            // 
            // PenSize_5
            // 
            PenSize_5.Name = "PenSize_5";
            PenSize_5.Size = new Size(86, 22);
            PenSize_5.Text = "5";
            PenSize_5.Click += PenSize_5_Click;
            // 
            // PenSize_10
            // 
            PenSize_10.Name = "PenSize_10";
            PenSize_10.Size = new Size(86, 22);
            PenSize_10.Text = "10";
            PenSize_10.Click += PenSize_10_Click;
            // 
            // PenSize_15
            // 
            PenSize_15.Name = "PenSize_15";
            PenSize_15.Size = new Size(86, 22);
            PenSize_15.Text = "15";
            PenSize_15.Click += PenSize_15_Click;
            // 
            // Board_PB
            // 
            Board_PB.BackColor = SystemColors.Control;
            Board_PB.Dock = DockStyle.Fill;
            Board_PB.Location = new Point(0, 55);
            Board_PB.Name = "Board_PB";
            Board_PB.Size = new Size(800, 395);
            Board_PB.SizeMode = PictureBoxSizeMode.AutoSize;
            Board_PB.TabIndex = 2;
            Board_PB.TabStop = false;
            Board_PB.MouseDown += Board_PB_MouseDown;
            Board_PB.MouseMove += Board_PB_MouseMove;
            Board_PB.MouseUp += Board_PB_MouseUp;
            // 
            // MyPaint
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(Board_PB);
            Controls.Add(toolStrip1);
            Controls.Add(menuStrip1);
            MainMenuStrip = menuStrip1;
            Name = "MyPaint";
            Text = "My Paint";
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            toolStrip1.ResumeLayout(false);
            toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)Board_PB).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripMenuItem Save_MenuItem;
        private ToolStripMenuItem Open_MenuItem;
        private ToolStrip toolStrip1;
        private ToolStripButton Black_Btn;
        private ToolStripButton Red_Btn;
        private ToolStripButton Gold_Btn;
        private ToolStripDropDownButton PenSize;
        private ToolStripMenuItem PenSize_5;
        private ToolStripMenuItem PenSize_10;
        private ToolStripMenuItem PenSize_15;
        private PictureBox Board_PB;
    }
}